package ru.kozlov.victor.instagram_app;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class DetailPhotoFragment extends Fragment {
    private Drawable drawable;
    private ImageView detailImageView;

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detail_foto, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        detailImageView = view.findViewById(R.id.detail_image_view);
        detailImageView.setImageDrawable(drawable);
    }
}
