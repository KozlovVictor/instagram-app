package ru.kozlov.victor.instagram_app;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder> {
    private static final String DELETE_IMAGE_DIALOG_TITLE = "Delete this image from list?";
    private static final String DELETE_BUTTON = "Delete";
    private Fragment fragment;
    private DetailPhotoFragment detailFragment;
    private AlertDialog.Builder deleteItem;
    private ArrayList<String> photosPath;

    PhotosAdapter(Fragment fragment, ArrayList<String> photosPath) {
        this.fragment = fragment;
        this.photosPath = photosPath;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycleview_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int position) {
        String filePath = photosPath.get(position);
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        if (bitmap != null) viewHolder.pImageView.setImageBitmap(bitmap);

        viewHolder.pImageView.setOnClickListener(view -> {
            if (fragment.getFragmentManager() != null) {
                fragment.getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frgContent, detailFragment = new DetailPhotoFragment())
                        .commit();
            }
            detailFragment.setDrawable(viewHolder.pImageView.getDrawable());
        });
        viewHolder.pImageView.setOnLongClickListener(view -> {
            deleteItem = new AlertDialog.Builder(fragment.getActivity());
            deleteItem.setTitle(DELETE_IMAGE_DIALOG_TITLE)
                    .setCancelable(true)
                    .setPositiveButton(DELETE_BUTTON, (dialogInterface, i) -> {
                        photosPath.remove(position);
                        notifyDataSetChanged();
                    });
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return photosPath.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView pImageView;

        ViewHolder(@NonNull View view) {
            super(view);
            pImageView = view.findViewById(R.id.foto_image_view);
        }
    }
}
