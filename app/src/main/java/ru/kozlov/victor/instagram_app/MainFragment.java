package ru.kozlov.victor.instagram_app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

public class MainFragment extends Fragment {
    public static final String PHOTO_ADDED = "Photo added";
    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final String DATE_TEMPLATE = "ddMMyyyy_HHmmss";
    private FloatingActionButton fab;
    private PhotosAdapter photosAdapter;
    private RecyclerView recyclerView;
    private ArrayList<String> photosPath;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewes(view);

        photosPath = new ArrayList<>();

        recyclerView.setHasFixedSize(true);
        photosAdapter = new PhotosAdapter(this, photosPath);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));
        recyclerView.setAdapter(photosAdapter);

        fab.setOnClickListener(view1 -> {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            if (imageBitmap != null && getView() != null) {
                try {
                    saveImageToStorage(imageBitmap);
                    Snackbar.make(getView(), PHOTO_ADDED, Snackbar.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void saveImageToStorage(Bitmap imageBitmap) throws IOException {
        File storageDirectory = Objects.requireNonNull(getActivity()).getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        String timeStamp = new SimpleDateFormat(DATE_TEMPLATE).format(new Date());
        StringBuilder imageFileName = new StringBuilder();
        imageFileName.append("IMG_");
        imageFileName.append(timeStamp);
        File.createTempFile(imageFileName.toString(), ".jpg", storageDirectory);
        FileOutputStream fileOutputStream;
        fileOutputStream = getActivity().openFileOutput(imageFileName.toString(), Context.MODE_PRIVATE);
        fileOutputStream.write(imageBitmap.getRowBytes());
        fileOutputStream.close();
        photosPath.add(imageFileName.toString());
        photosAdapter.notifyDataSetChanged();
    }

    void initViewes(@NonNull View view) {
        fab = view.findViewById(R.id.fab);
        recyclerView = view.findViewById(R.id.rvItems);
    }
}
